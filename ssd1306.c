/*
 *
 * libio
 * ssd1306.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <stdint.h>

#include "i2c.h"
#include "ssd1306.h"

static void ssd1306_send(uint8_t addr, uint8_t cmd)
{
	uint8_t data[2];

	data[0] = 0;
	data[1] = cmd;

	i2c_write(addr, data, 2);
}

void ssd1306_update(uint8_t addr, const uint8_t *buf)
{
	uint8_t i, j, packet[SSD1306_SCANLINE + 1];

	ssd1306_send(addr, SSD1306_COLUMN);
	ssd1306_send(addr, 0);
	ssd1306_send(addr, SSD1306_WIDTH - 1);

	ssd1306_send(addr, SSD1306_PAGE);
	ssd1306_send(addr, 0);
	ssd1306_send(addr, 0x07);

	packet[0] = 0x40;

	for (i = 0; i < SSD1306_BUFFER / SSD1306_SCANLINE; i++) {
		for (j = 0; j < SSD1306_SCANLINE; j++)
			packet[j + 1] = buf[i * 16 + j];

		i2c_write(addr, packet, SSD1306_SCANLINE + 1);
	}
}

void ssd1306_init(uint8_t addr)
{
	ssd1306_send(addr, SSD1306_DISPLAY_OFF);

	ssd1306_send(addr, SSD1306_CLOCK_DIVISOR);
	ssd1306_send(addr, 128);

	ssd1306_send(addr, SSD1306_MULTIPLEX);
	ssd1306_send(addr, SSD1306_HEIGHT - 1);

	ssd1306_send(addr, SSD1306_OFFSET);
	ssd1306_send(addr, 0);

	ssd1306_send(addr, SSD1306_OFFSET_START);

	ssd1306_send(addr, SSD1306_CHARGEPUMP);
	ssd1306_send(addr, 0x14);

	ssd1306_send(addr, SSD1306_MODE);
	ssd1306_send(addr, 0);

	ssd1306_send(addr, SSD1306_REMAP | 1);

	ssd1306_send(addr, SSD1306_COMSCANDEC);

	ssd1306_send(addr, SSD1306_COMPINS);
	ssd1306_send(addr, 0x12);

	ssd1306_send(addr, SSD1306_CONTRAST);
	ssd1306_send(addr, 0xCF);

	ssd1306_send(addr, SSD1306_PRECHARGE);
	ssd1306_send(addr, 0xF1);

	ssd1306_send(addr, SSD1306_VCOMDETECT);
	ssd1306_send(addr, 0x40);

	ssd1306_send(addr, SSD1306_DISPLAY_RESUME);
	ssd1306_send(addr, SSD1306_DISPLAY_NORMAL);
	ssd1306_send(addr, SSD1306_DISPLAY_ON);
}
