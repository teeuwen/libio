/*
 *
 * libio
 * hc585.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <util/delay.h>

#include "hc595.h"
#include "io.h"

void hc595_write(struct hc595 *hc595, uint8_t data)
{
	int i;

	for (i = 0; i < HC595_OUTPUTS; i++) {
		io_set(hc595->sdi_p, hc595->sdi_o, (data << i) & 0x80);

		io_set(hc595->shcp_p, hc595->shcp_o, 1);
		_delay_ms(1);
		io_set(hc595->shcp_p, hc595->shcp_o, 0);
	}

	io_set(hc595->stcp_p, hc595->stcp_o, 1);
	_delay_ms(1);
	io_set(hc595->stcp_p, hc595->stcp_o, 0);
}

void hc595_clear(struct hc595 *hc595)
{
	int i;

	for (i = 0; i < HC595_OUTPUTS; i++) {
		io_set(hc595->sdi_p, hc595->sdi_o, 0);

		io_set(hc595->shcp_p, hc595->shcp_o, 1);
		_delay_ms(1);
		io_set(hc595->shcp_p, hc595->shcp_o, 0);
	}
}

struct hc595 *hc595_init(int sdi_p, uint8_t sdi_o,
		int stcp_p, uint8_t stcp_o
		int shcp_p, uint8_t shcp_o)
{
	io_output(sdi_p, sdi_o);
	io_output(stcp_p, stcp_o);
	io_output(shcp_p, shcp_o);

	io_set(sdi_p, sdi_o, 0);
	io_set(stcp_p, stcp_o, 0);
	io_set(shcp_p, shcp_o, 0);
}
