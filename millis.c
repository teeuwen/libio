/*
 *
 * libio
 * millis.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <avr/interrupt.h>

static volatile uint64_t uptime;

uint64_t millis(void)
{
	return uptime;
}

void millis_init(void)
{
	if (uptime)
		return;

	cli();

	/* Set normal mode on TIMER 0 (ATmega 2560 17.11.1) */
	TCCR0A = 0;
	/* Set F_CPU/8 prescaler (ATmega 2560 17.11.5) */
	TCCR0B = (1 << CS01);
	/* Enable OVF interrupt (ATmega 2560 17.11.36) */
	TIMSK0 = (1 << TOIE0);

	sei();
}

ISR(TIMER0_OVF_vect)
{
	uptime++;
}
