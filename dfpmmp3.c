/*
 *
 * libio
 * dfpmmp3.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <avr/io.h>

/*
 * XXX
 * This library is feature incomplete and currently only works on
 * hardware serial port 1.
 */

static void dfpmmp3_checksum(uint8_t *buf)
{
	uint16_t c = 0;
	int i;

	for (i = 1; i < 7; i++)
		c += buf[i];

	buf[7] = -c >> 8;
	buf[8] = -c & 0xFF;
}

static void dfpmmp3_write(uint8_t cmd, uint16_t args)
{
	uint8_t buf[10];
	int i;

	buf[0] = 0x7E;
	buf[1] = 0xFF;
	buf[2] = 0x06;
	buf[3] = cmd;
	buf[4] = 0;
	buf[5] = args >> 8;
	buf[6] = args & 0xFF;
	dfpmmp3_checksum(buf);
	buf[9] = 0xEF;

	for (i = 0; i < 10; i++) {
		while (!(UCSR1A & (1 << UDRE1)));
		UDR1 = (uint8_t) buf[i];
	}
}

void dfpmmp3_play(uint16_t n)
{
	dfpmmp3_write(0x12, n);
}

void dfpmmp3_stop(void)
{
	dfpmmp3_write(0x16, 0);
}

void dfpmmp3_init(void)
{
	UBRR1 = F_CPU / (9600 * 16L) - 1;
	UCSR1B = (1 << RXEN1) | (1 << TXEN1);
	UCSR1C = (1 << 2) | (1 << 1);
}
