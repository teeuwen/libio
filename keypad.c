/*
 *
 * libio
 * keypad.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "io.h"
#include "keypad.h"
#include "millis.h"

static volatile uint64_t time_last;

int keypad_getch(const struct keypad *kpd)
{
	int i, j;

	if (millis() - time_last <= KEYPAD_DEBOUNCE)
		return -1;

	time_last = millis();

	for (i = 0; i < kpd->cols; i++) {
		io_output(kpd->port_cols[i], kpd->off_cols[i]);
		io_set(kpd->port_cols[i], kpd->off_cols[i], 0);

		for (j = 0; j < kpd->rows; j++)
			if (!io_get(kpd->port_rows[j], kpd->off_rows[j])) {
				io_set(kpd->port_cols[i], kpd->off_cols[i], 1);
				io_input(kpd->port_cols[i], kpd->off_cols[i]);
				return i + j * kpd->cols;
			}

		io_set(kpd->port_cols[i], kpd->off_cols[i], 1);
		io_input(kpd->port_cols[i], kpd->off_cols[i]);
	}

	return -1;
}

void keypad_init(const struct keypad *kpd)
{
	int i;

	for (i = 0; i < kpd->rows; i++) {
		io_input(kpd->port_rows[i], kpd->off_rows[i]);
		io_set(kpd->port_rows[i], kpd->off_rows[i], 1);
	}

	millis_init();
}
