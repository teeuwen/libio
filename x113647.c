/*
 *
 * libio
 * x113647.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <avr/interrupt.h>
#include <util/delay.h> /* TEMP? */

#include "io.h"
#include "x113647.h"

static void _x113647_step(const struct x113647 *x113647, int i)
{
	io_set(x113647->port_in1, x113647->off_in1, (1 << i) & 0x01);
	io_set(x113647->port_in2, x113647->off_in2, (1 << i) & 0x02);
	io_set(x113647->port_in3, x113647->off_in3, (1 << i) & 0x04);
	io_set(x113647->port_in4, x113647->off_in4, (1 << i) & 0x08);
}

void x113647_step(const struct x113647 *x113647, int steps)
{
	int i;

	for (i = 0; i < steps; i++) {
		_x113647_step(x113647, 0);
		_delay_ms(3);
		_x113647_step(x113647, 1);
		_delay_ms(3);
		_x113647_step(x113647, 2);
		_delay_ms(3);
		_x113647_step(x113647, 3);
		_delay_ms(3);
	}

	io_set(x113647->port_in1, x113647->off_in1, 0);
	io_set(x113647->port_in2, x113647->off_in2, 0);
	io_set(x113647->port_in3, x113647->off_in3, 0);
	io_set(x113647->port_in4, x113647->off_in4, 0);

	/* TCCR2B =  */
}

void x113647_init(const struct x113647 *x113647)
{
	io_output(x113647->port_in1, x113647->off_in1);
	io_output(x113647->port_in2, x113647->off_in2);
	io_output(x113647->port_in3, x113647->off_in3);
	io_output(x113647->port_in4, x113647->off_in4);

	io_set(x113647->port_in1, x113647->off_in1, 0);
	io_set(x113647->port_in2, x113647->off_in2, 0);
	io_set(x113647->port_in3, x113647->off_in3, 0);
	io_set(x113647->port_in4, x113647->off_in4, 0);

	cli();

	/* Set normal mode on TIMER 2 (AVR 16.11.1) */
	TCCR2A = 0;
	/* Disable clock source (AVR 16.11.2) */
	TCCR2B = 0;
	/* Enable OVF interrupt (AVR 16.11.8) */
	TIMSK2 = (1 << TOIE2);

	sei();
}
