/*
 *
 * libio
 * ssd1306.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _LIBIO_SSD1306_H
#define _LIBIO_SSD1306_H

#define SSD1306_WIDTH		128
#define SSD1306_HEIGHT		64

#define SSD1306_BUFFER		((SSD1306_WIDTH * SSD1306_HEIGHT) / 8)
#define SSD1306_SCANLINE	16

#define SSD1306_CHARGEPUMP	0x8D
#define SSD1306_CLOCK_DIVISOR	0xD5
#define SSD1306_COLUMN		0x21
#define SSD1306_COMPINS		0xDA
#define SSD1306_COMSCANDEC	0xC8
#define SSD1306_CONTRAST	0x81
#define SSD1306_DISPLAY_NORMAL	0xA6
#define SSD1306_DISPLAY_OFF	0xAE
#define SSD1306_DISPLAY_ON	0xAF
#define SSD1306_DISPLAY_RESUME	0xA4
#define SSD1306_MODE		0x20
#define SSD1306_MULTIPLEX	0xA8
#define SSD1306_OFFSET		0xD3
#define SSD1306_OFFSET_START	0x40
#define SSD1306_PAGE		0x22
#define SSD1306_PRECHARGE	0xD9
#define SSD1306_REMAP		0xA0
#define SSD1306_VCOMDETECT	0xDB

void ssd1306_update(uint8_t addr, const uint8_t *buf);

void ssd1306_init(uint8_t addr);

#endif
