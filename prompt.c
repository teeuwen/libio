/*
 *
 * libio
 * prompt.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <avr/io.h>

#include <stdio.h>

#include "uart.h"

static int line_index;

static int prompt_putch(char c, FILE *s)
{
	if (c == '\n') {
		prompt_putch('\r', s);
		line_index = 0;
	} else if (c == '\b') {
		if (line_index < 1)
			return 0;

		uart_write('\b');
		uart_write(' ');

		line_index--;
	} else {
		line_index++;
	}

	uart_write(c);

	return 0;
}

static int prompt_getch(FILE *s)
{
	register int c;

	c = uart_read();

	if (c == '\r')
		c = '\n';

	prompt_putch((char) c, stdout);

	return c;
}

void prompt_init(uint32_t baud)
{
	uart_init(baud);

	stdin = fdevopen(0, &prompt_getch);
	stdout = fdevopen(&prompt_putch, 0);

	line_index = 0;
}
