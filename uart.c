/*
 *
 * libio
 * uart.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <avr/io.h>

uint8_t uart_read(void)
{
	/* Wait for data (ATmega 328 20.7.1) */
	while (!(UCSR0A & (1 << RXC0)));

	return UDR0;
}

void uart_write(uint8_t data)
{
	/* Wait for empty transmit buffer (ATmega 328 20.6.1) */
	while (!(UCSR0A & (1 << UDRE0)));

	UDR0 = data;
}

void uart_init(uint32_t baud)
{
	/* Set transmission rate (ATmega 328 20.3.1) */
	UBRR0 = F_CPU / (baud * 16L) - 1;
	/* Enable RX and TX (ATmega 328 21.8.3) */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
#ifdef __AVR_ATmega2560__
	/* Set little endian and clock phase (ATmega 2560 22.10.4) */
	UCSR0C = (1 << 2) | (1 << 1);
#else
	/* Set little endian and clock phase (ATmega 328 21.8.4) */
	UCSR0C = (1 << UDORD0) | (1 << UCPHA0);
#endif
}
