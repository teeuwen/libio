/*
 *
 * libio
 * io.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include "io.h"

void _io_ddr(int port, uint8_t off, int value)
{
	volatile uint8_t *ddr;

	switch (port) {
#ifdef __AVR_ATmega2560__
	case PORT_A:
		ddr = &DDRA;
		break;
#endif
	case PORT_B:
		ddr = &DDRB;
		break;
	case PORT_C:
		ddr = &DDRC;
		break;
	case PORT_D:
		ddr = &DDRD;
		break;
#ifdef __AVR_ATmega2560__
	case PORT_E:
		ddr = &DDRE;
		break;
	case PORT_F:
		ddr = &DDRF;
		break;
	case PORT_G:
		ddr = &DDRG;
		break;
	case PORT_H:
		ddr = &DDRH;
		break;
	case PORT_J:
		ddr = &DDRJ;
		break;
	case PORT_K:
		ddr = &DDRK;
		break;
	case PORT_L:
		ddr = &DDRL;
		break;
#endif
	default:
		return;
	}

	if (value)
		*ddr |= (1 << off);
	else
		*ddr &= ~(1 << off);
}

void _io_port(int port, uint8_t off, int value)
{
	volatile uint8_t *_port;

	switch (port) {
#ifdef __AVR_ATmega2560__
	case PORT_A:
		_port = &PORTA;
		break;
#endif
	case PORT_B:
		_port = &PORTB;
		break;
	case PORT_C:
		_port = &PORTC;
		break;
	case PORT_D:
		_port = &PORTD;
		break;
#ifdef __AVR_ATmega2560__
	case PORT_E:
		_port = &PORTE;
		break;
	case PORT_F:
		_port = &PORTF;
		break;
	case PORT_G:
		_port = &PORTG;
		break;
	case PORT_H:
		_port = &PORTH;
		break;
	case PORT_J:
		_port = &PORTJ;
		break;
	case PORT_K:
		_port = &PORTK;
		break;
	case PORT_L:
		_port = &PORTL;
		break;
#endif
	default:
		return;
	}

	if (value)
		*_port |= (1 << off);
	else
		*_port &= ~(1 << off);
}

int _io_pin(int port, uint8_t off)
{
	volatile uint8_t *pin;

	switch (port) {
#ifdef __AVR_ATmega2560__
	case PORT_A:
		pin = &PINA;
		break;
#endif
	case PORT_B:
		pin = &PINB;
		break;
	case PORT_C:
		pin = &PINC;
		break;
	case PORT_D:
		pin = &PIND;
		break;
#ifdef __AVR_ATmega2560__
	case PORT_E:
		pin = &PINE;
		break;
	case PORT_F:
		pin = &PINF;
		break;
	case PORT_G:
		pin = &PING;
		break;
	case PORT_H:
		pin = &PINH;
		break;
	case PORT_J:
		pin = &PINJ;
		break;
	case PORT_K:
		pin = &PINK;
		break;
	case PORT_L:
		pin = &PINL;
		break;
#endif
	default:
		return 0;
	}

	return *pin & (1 << off);
}

uint32_t io_read(uint8_t adc)
{
	int res;

	/* Use AREF and select ADC (ATmega 328 24.9.1) */
	ADMUX = (1 << REFS0) | (adc & 0x0F);

	/* Enable and start ADC (ATmega 328 24.9.2) */
	ADCSRA = (1 << ADEN) | (1 << ADSC);

	/* Wait for conversion to finish (ATmega 328 ) */
	while (!(ADCSRA & (1 << ADIF)));

	res = (ADCH << 8) | (ADCL & 0x0F);

	return res;
}
