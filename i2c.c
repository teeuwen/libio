/*
 *
 * libio
 * i2c.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/twi.h>

#include "i2c.h"

static void (*rx_handler) (uint8_t data, size_t i);
static uint8_t (*tx_handler) (size_t i);
static size_t bufi;

static int i2c_start(uint8_t addr, int read)
{
	/* Send start operation (ATmega 328 22.6, 22.9.2) */
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);

	/* Wait for TWINT (ATmega 328 22.5.5, 22.6, 22.9.2) */
	while (!(TWCR & (1 << TWINT)));

	/* Check if status is OK (ATmega 328 22.6) */
	if ((TWSR & 0xF8) != TW_START)
		return 0;

	/* Write address to data register (ATmega 328 22.6, 22.9.4)
	 * and optionally set read bit (ATmega 328 22.3.3)
	 */
	TWDR = (addr << 1) | read;
	/* Set interrupts and enable TWI (ATmega 328 22.6, 22.9.2) */
	TWCR = (1 << TWINT) | (1 << TWEN);

	/* Wait for TWINT (ATmega 328 22.5.5, 22.6, 22.9.2) */
	while (!(TWCR & (1 << TWINT)));

	/* Check if status is OK (ATmega 328 22.6) */
	return ((TWSR & 0xF8) == (read ? TW_MR_SLA_ACK : TW_MT_SLA_ACK));
}

static void i2c_stop(void)
{
	/* Send stop operation (ATmega 328 22.6, 22.9.2) */
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);

	/* Wait for stop to finish */
	while (TWCR & (1 << TWSTO));
}

static uint8_t i2c_readb(int last)
{
	/* Set interrupts and enable TWI (ATmega 328 22.6, 22.9.2) */
	TWCR = (1 << TWINT) | (1 << TWEN) | (!last << TWEA);

	/* Wait for TWINT (ATmega 328 22.5.5, ATmega 328 22.6, 22.9.2) */
	while (!(TWCR & (1 << TWINT)));

	return TWDR;
}

static int i2c_writeb(uint8_t data)
{
	/* Write data to data reg. (ATmega 328 22.6, 22.9.4) */
	TWDR = data;
	/* Set interrupts and enable TWI (ATmega 328 22.6, 22.9.2) */
	TWCR = (1 << TWINT) | (1 << TWEN);

	/* Wait for TWINT (ATmega 328 22.5.5, ATmega 328 22.6, 22.9.2) */
	while (!(TWCR & (1 << TWINT)));

	/* Check if status is OK (ATmega 328 22.6) */
	return ((TWSR & 0xF8) == TW_MT_DATA_ACK);
}

int i2c_read(uint8_t addr, uint8_t *buf, size_t n)
{
	size_t i;

	if (!i2c_start(addr, 1))
		return 0;

	for (i = 0; i < n - 1; i++)
		buf[i] = i2c_readb(0);
	buf[i] = i2c_readb(1);

	i2c_stop();

	return n;
}

int i2c_write(uint8_t addr, uint8_t *data, size_t n)
{
	size_t i;

	if (!i2c_start(addr, 0))
		return 0;

	for (i = 0; i < n; i++)
		if (!i2c_writeb(data[i]))
			return i;

	i2c_stop();

	return n;
}

uint8_t i2c_scan(uint8_t lower_addr, uint8_t upper_addr)
{
	uint8_t i, n;

	for (i = lower_addr, n = 0; i <= upper_addr; i++) {
		if (i2c_start(i, 0))
			n++;

		i2c_stop();
	}

	return n;
}

static void i2c_init(void)
{
	/* Set TWI prescaler to 1 (ATmega 328 22.9.3) */
	TWSR = 0;
	/* Set TWI bit rate (ATmega 328 22.9.1) */
	TWBR = (F_CPU / F_SCL - 16) >> 1;
}

void i2c_init_master(void)
{
	i2c_init();
}

void i2c_init_slave(uint8_t addr, void (*rx) (uint8_t data, size_t i),
		uint8_t (*tx) (size_t i))
{
	cli();

	i2c_init();

	/* Initialize TWAR for slave mode (ATmega 328 22.7.3, 22.7.4) */
	TWAR = (addr << 1) | 1;
	/* Initialize TWCR for slave mode (ATmega 328 22.7.3, 22.7.4) */
	TWCR = (1 << TWINT) | (1 << TWEA) | (1 << TWEN) | (1 << TWIE);

	rx_handler = rx;
	tx_handler = tx;

	sei();
}

ISR(TWI_vect)
{
	switch (TWSR & 0xF8) {
	case TW_SR_SLA_ACK:
	case TW_SR_ARB_LOST_SLA_ACK:
	case TW_SR_GCALL_ACK:
	case TW_SR_ARB_LOST_GCALL_ACK:
		bufi = 0;

		/* Initialize TWCR for slave mode (ATmega 328 22.7.3, 22.7.4) */
		TWCR = (1 << TWINT) | (1 << TWEA) | (1 << TWEN) | (1 << TWIE);
		break;
	case TW_SR_DATA_ACK:
	case TW_SR_GCALL_DATA_ACK:
		if (rx_handler)
			rx_handler(TWDR, bufi++);
		/* Fallthrough */
	case TW_SR_STOP:
		/* Initialize TWCR for slave mode (ATmega 328 22.7.3, 22.7.4) */
		TWCR = (1 << TWINT) | (1 << TWEA) | (1 << TWEN) | (1 << TWIE);
		break;
	case TW_SR_DATA_NACK:
	case TW_SR_GCALL_DATA_NACK:
	case TW_ST_LAST_DATA:
	case 0:
		i2c_stop();
		break;
	case TW_ST_SLA_ACK:
	case TW_ST_ARB_LOST_SLA_ACK:
		bufi = 0;
		/* Fallthrough */
	case TW_ST_DATA_ACK:
		if (tx_handler)
			TWDR = tx_handler(bufi++);
		else
			TWDR = 0;
		/* Fallthrough */
	case TW_ST_DATA_NACK:
	default:
		/* Initialize TWCR for slave mode (ATmega 328 22.7.3, 22.7.4) */
		TWCR = (1 << TWINT) | (1 << TWEA) | (1 << TWEN) | (1 << TWIE);
		break;
	}
}
