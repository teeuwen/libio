/*
 *
 * libio
 * io.h
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef _LIBIO_IO_H
#define _LIBIO_IO_H

#include <avr/io.h>

#ifdef __AVR_ATmega2560__
#define PORT_A	0
#endif

#define PORT_B	1
#define PORT_C	2
#define PORT_D	3

#ifdef __AVR_ATmega2560__
#define PORT_E	4
#define PORT_F	5
#define PORT_G	6
#define PORT_H	7
#define PORT_J	8
#define PORT_K	9
#define PORT_L	10
#endif

void _io_ddr(int port, uint8_t off, int data);
void _io_port(int port, uint8_t off, int data);
int _io_pin(int port, uint8_t off);

#define io_input(p, o)	_io_ddr(p, o, 0)
#define io_output(p, o)	_io_ddr(p, o, 1)

#define io_get(p, o)	_io_pin(p, o)
#define io_set(p, o, v)	_io_port(p, o, v)

#define IO_INPUT(io)	io_input(io)
#define IO_OUTPUT(io)	io_output(io)

#define IO_GET(io)	io_get(io)
#define IO_SET(io, v)	io_set(io, v)

uint32_t io_read(uint8_t adc);

#endif
