/*
 *
 * libio
 * ir.c
 *
 * Copyright (C) 2017 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <avr/interrupt.h>

/* FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME */
#include <util/delay.h>

#include "io.h"
#include "ir.h"

static volatile uint32_t ir_buf[IR_BUFFER], ir_timer;
static volatile unsigned char ir_bufi, ir_status;

static int ir_port;
static uint8_t ir_off;

static int ir_compare(uint32_t old, uint32_t new)
{
	if (new < old * .8)
		return 0;
	if (old < new * .8)
		return 2;
	return 1;
}

uint32_t ir_read(void)
{
	long hash;
	int i;

	if (ir_status != IR_S_FINI)
		return 0;

	if (ir_bufi < IR_MIN) {
		ir_status = IR_S_IDLE;
		return 0;
	}

	/*
	 * Algorithm taken from
	 * https://github.com/z3t0/Arduino-IRremote/blob/master/irRecv.cpp
	 * Copyright 2009 Ken Shirriff
	 */

	for (i = 1, hash = 2166136261; (i + 2) < ir_bufi; i++)
		hash = (hash * 16777619) ^ ir_compare(ir_buf[i], ir_buf[i + 2]);

	/* FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME */
	_delay_ms(150);

	ir_status = IR_S_IDLE;

	return hash;
}

void ir_init(int port, uint8_t off)
{
	ir_port = port;
	ir_off = off;

	io_input(port, off);
	io_set(port, off, 1);

	cli();

	/* Set CTC mode on TIMER 2 (ATmega 328 18.11.1) */
	TCCR2A = (1 << WGM21);
	/* Set F_CPU/8 prescaler (ATmega 328 18.11.2) */
	TCCR2B = (1 << CS21);
	/* Enable COMPA interrupt (ATmega 328 18.11.6) */
	TIMSK2 = (1 << OCIE2A);
	/* Set COMPA register (ATmega 328 18.11.4) */
	OCR2A = (F_CPU * IR_PRECISION / 1000000) >> 3;
	/* Clear TIMER 2 (ATmega 328 18.11.3) */
	TCNT2 = 0;

	sei();
}

ISR(TIMER2_COMPA_vect)
{
	ir_timer++;

	if (ir_bufi > IR_BUFFER) {
		ir_status = IR_S_IDLE;

		ir_bufi = 0;
		ir_timer = 0;
		return;
	}

	switch (ir_status) {
	case IR_S_IDLE:
		if (io_get(ir_port, ir_off))
			break;

		if (ir_timer > (5000 / IR_PRECISION)) {
			ir_status = IR_S_INIT;

			ir_bufi = 1;
			ir_buf[ir_bufi] = ir_timer;
		}

		ir_timer = 0;

		break;
	case IR_S_INIT:
		if (io_get(ir_port, ir_off)) {
			ir_status = IR_S_READ;

			ir_buf[ir_bufi++] = ir_timer;
			ir_timer = 0;
		}

		break;
	case IR_S_READ:
		if (!io_get(ir_port, ir_off)) {
			ir_status = IR_S_INIT;

			ir_buf[ir_bufi++] = ir_timer;
			ir_timer = 0;
		} else if (ir_timer > (5000 / IR_PRECISION)) {
			ir_status = IR_S_FINI;
		}

		break;
	case IR_S_FINI:
		if (!io_get(ir_port, ir_off))
			ir_timer = 0;

		break;
	}
}
